# Heatwave

> sss

GitLab for collaboration

- registry
- pipelines
- code

## Prerequisites

Requires a working MicroK8s cluster behind an Internet-connected router, and a domain name.

## Design


## Develop and Deploy

``` bash
pulumi config set kubernetes:context omega # or
pulumi config set kubernetes:context live
```

Pre-requisites for Helm.

``` bash
helm repo add gitlab https://charts.gitlab.io/
helm repo update
```

### Pulumi Stack Configurations

``` bash
pulumi config set --plaintext tld deposition.cloud # replace with your domain
```

### GitLab



### Deploy

``` bash
pulumi up -f -y
```

We should now be able to securely access: [nginx.deposition.cloud](https://nginx.deposition.cloud)

Yey! :fireworks:

## Troubleshooting

Many things could go wrong. :smile:
