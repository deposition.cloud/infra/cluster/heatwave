import * as pulumi from "@pulumi/pulumi"
import * as k8s from "@pulumi/kubernetes"

const http = 80
const https = 443

interface Features {
  certManager: boolean
}

let config = new pulumi.Config()

const stack = pulumi.getStack()
const tier = pulumi.getProject()

const features = config.requireObject<Features>('features')

const org = config.require("org")
const domain = config.require("domain")
const adminEmail = config.requireSecret('adminEmail')
const externalIP = config.require("externalIP")

const fauna = new pulumi.StackReference(`${org}/fauna/${stack}`)

const deps = {
  fauna: fauna.requireOutput('default')
}

const out: any = {}

console.log(`Booting up ${domain}...`)

/*
 * GitLab
 */

const gitlabShortName = 'lab'
const gitlabNamespace = new k8s.core.v1.Namespace(gitlabShortName, {
  metadata: { labels: { name: gitlabShortName, stack, tier } }
})
const gitlabNamespaceName = gitlabNamespace.metadata.name

const postgresSecretName = 'postgres'
const postgresKey = 'password'
const postgresSecret = new k8s.core.v1.Secret(postgresSecretName, {
  metadata: {
    namespace: gitlabNamespaceName
  },
  stringData: {
    [postgresKey]: config.requireSecret('postgresPassword')
  }
})


const gitlabHost = `gitlab.${domain}`
out.gitlab = `https://${gitlabHost}`

const gitlab = new k8s.helm.v3.Chart(gitlabShortName, {
  repo: "gitlab",
  chart: "gitlab",
  namespace: gitlabNamespaceName,
  values: {
    certmanager: { install: false },
    "nginx-ingress": { enabled: false },
    postgresql: { install: false },
    global: {
      edition: "ce",
      email: {
        display_name: `${domain} GitLab`,
        from: adminEmail,
        reply_to: adminEmail
      },
      hosts: { 
        domain,
        externalIP,
      },
      ingress: {
        configureCertmanager: false,
        class: "nginx"
      },
      psql: {
        host: deps.fauna.apply(o => o.out.postgresHost),
        password: {
          secret: postgresSecretName,
          key: postgresKey
        },
        username: config.require('postgresUsername')
      }
    },
    "gitlab-runner": {
      certsSecretName: "gitlab-wildcard-tls-chain"
    }
  }
})

// const gitlabIngress = new k8s.networking.v1beta1.Ingress('ingress', {
//   metadata: {
//     namespace: gitlabNamespaceName,
//     annotations: {
//       'kubernetes.io/ingress.class': 'nginx',
//       // Add the following line (staging first for testing, then apply the prod issuer)
//       'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
//     }
//   },
//   spec: {
//     tls: [{
//       hosts: [gitlabHost],
//       secretName: 'default-tls-secret'
//     }],
//     rules: [{
//       host: gitlabHost,
//       http: {
//         paths: [{
//           backend: { serviceName: 'web', servicePort: 8084 },
//           path: '/',
//           pathType: 'ImplementationSpecific'
//         }]
//       }
//     }]
//   }
// }, {
//   dependsOn: [gitlab]
// })

export default {
  features,
  out,
  deps
}